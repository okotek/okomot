module okomot

go 1.22.0

require (
	gitlab.com/okotek/okoerr v0.0.1
	gitlab.com/okotek/okoframe v0.0.1
	gitlab.com/okotek/okonet v0.0.1
)

require gocv.io/x/gocv v0.35.0 // indirect

replace gitlab.com/okotek/okoerr => ../okoerr

replace gitlab.com/okotek/okoframe => ../okoframe

replace gitlab.com/okotek/okonet => ../okonet
