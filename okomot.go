package main

import (
	"flag"
	"sync"

	"gitlab.com/okotek/okoerr"
	"gitlab.com/okotek/okoframe"
	"gitlab.com/okotek/okonet"
)

type mutexedMap struct {
	FrameMap map[string]okoframe.Frame
	Mtx      sync.Mutex
}

func main() {

	sendTarg := flag.String("sendoffTarg", "localhost:8083", "Target we're sending our processed frames to.")
	flag.Parse()

	lnChan := make(chan okoframe.Frame)
	postChan := make(chan okoframe.Frame)

	go okonet.Listen(lnChan, ":8082")
	go manageCompMap(lnChan, postChan, 1)
	go okonet.Sendoff(postChan, *sendTarg)

	var wg sync.WaitGroup
	wg.Add(147297)
	wg.Wait()
}

func manageCompMap(input, output chan okoframe.Frame, compDepth int) {
	var motMap = make(map[string]okoframe.Frame)
	var mtx sync.Mutex

	var compMap mutexedMap = mutexedMap{
		motMap,
		mtx,
	}

	for tFrm := range input {

		go func() {
			compMap.Mtx.Lock()
			comp, ok := compMap.FrameMap[tFrm.GetCamAndOwner()]
			compMap.Mtx.Unlock()

			if ok {
				if err := tFrm.DetectMotionGlobal(comp, compDepth); err != nil {
					okoerr.ReportError("failed to detect motion appropriately", "comp", err)
				}

				compMap.Mtx.Lock()
				compMap.FrameMap[tFrm.GetCamAndOwner()].BlendIn(tFrm)
				compMap.Mtx.Unlock()

			} else {
				newCompFrame := tFrm
				compMap.Mtx.Lock()
				compMap.FrameMap[newCompFrame.GetCamAndOwner()] = newCompFrame
				compMap.Mtx.Unlock()
			}
		}()
	}
}
